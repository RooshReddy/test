﻿using UnityEngine;
using System.Collections;
using UnityEngine.Windows.Speech;
using System;
using System.Text;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private string[] m_Keywords;

    private KeywordRecognizer m_Recognizer;

    private Question question;

    public TextMesh questionText;
    public TextMesh[] answerChoices;


    public TextMesh questionNumberText;

    public static GameManager instance;
    public int questionNumber = 1;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //get a question
        question = GetNextQuestion();
        m_Keywords = question.answerChoices;
        questionNumberText.text = questionNumber.ToString() + "/10";

        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        m_Recognizer.Start();
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        StringBuilder builder = new StringBuilder();
        builder.AppendFormat("{0} ({1}){2}", args.text, args.confidence, Environment.NewLine);
        builder.AppendFormat("\tTimestamp: {0}{1}", args.phraseStartTime, Environment.NewLine);
        builder.AppendFormat("\tDuration: {0} seconds{1}", args.phraseDuration.TotalSeconds, Environment.NewLine);
        Debug.Log(builder.ToString());
        if (args.confidence == ConfidenceLevel.Medium || args.confidence == ConfidenceLevel.High)
        {
            if (args.text == question.answerChoices[question.answerIndex])
            {
                Debug.Log("You got the correct answer");
                for (int i = 0; i < answerChoices.Length; i++)
                {
                    if (answerChoices[i].text == args.text)
                    {
                        answerChoices[i].color = Color.green;
                    }
                }
                Invoke("SetNextQuestion", 1f);
            }
            else
            {
                for (int i = 0; i < answerChoices.Length; i++)
                {
                    if (answerChoices[i].text == args.text)
                    {
                        answerChoices[i].color = Color.red;
                    }
                }
                Debug.Log("Nope");
            }
        }
    }


    public void SetNextQuestion()
    {
        Debug.Log("Setting next question");
        questionNumber++;
        questionNumberText.text = questionNumber.ToString() + "/10";
        question = GetNextQuestion();
        questionText.text = question.questionString;

        for (int i = 0; i < answerChoices.Length; i++)
        {
            answerChoices[i].text = question.answerChoices[i];
            answerChoices[i].color = Color.white;
        }

        m_Keywords = question.answerChoices;
        m_Recognizer.OnPhraseRecognized -= OnPhraseRecognized;
        m_Recognizer.Dispose();

        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.Start();
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;


    }




    public Question GetNextQuestion()
    {
        Question q = new Question();
        switch (questionNumber)
        {
            case 1:
                q.questionString = "If you checked the time on big ben, in which country would you be?";
                q.answerChoices = new string[] { "Australia", "South Africa", "England", "France" };
                q.answerIndex = 1;
                break;
            case 2:
                q.questionString = "Electronic music producer Kygo's popularity skyrocketed after a certain remix. Which song did he remix?";
                q.answerChoices = new string[] { "Gerli", "Striped Mountain Cat", "No Such Animal", "Liger" };
                q.answerIndex = 0;
                break;
            case 3:
                q.questionString = "Hitler was born in?";
                q.answerChoices = new string[] { "Poland", "Germany", "Austria", "Ukraine" };
                q.answerIndex = 0;
                break;
            case 4:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 5:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 6:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 7:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 8:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 9:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            case 10:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;
            default:
                q.questionString = "How many children does the average American family consist of?";
                q.answerChoices = new string[] { "1", "2", "3", "4" };
                q.answerIndex = 0;
                break;

        }
        return q;
    }



}

public class Question
{

    public string[] answerChoices;
    public int answerIndex;

    public string questionString;

}

//https://opentdb.com/
public class Result
{
    public string category { get; set; }
    public string correct_answer { get; set; }
    public string difficulty { get; set; }
    public List<string> incorrect_answers { get; set; }
    public string question { get; set; }
    public string type { get; set; }
}

public class RootObject
{
    public int response_code { get; set; }
    public List<Result> results { get; set; }
}