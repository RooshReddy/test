﻿using UnityEngine;
using System.Collections;

public class ViveManager : MonoBehaviour {

    public GameObject head;
    public GameObject leftHand;
    public GameObject rightHand;

    public static ViveManager instance;
    
    void Awake()
    {
        instance = this;
    }

    void OnDestroy()
    {
        if (instance == this)
            instance = null;
    }
}
