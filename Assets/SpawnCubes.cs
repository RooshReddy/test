﻿using UnityEngine;
using System.Collections;

public class SpawnCubes : MonoBehaviour
{

    public GameObject cubePrefab;

    void Update()
    {
        var lDevice = SteamVR_Controller.Input((int)ViveManager.instance.leftHand.GetComponent<SteamVR_TrackedObject>().index);
        var rDevice = SteamVR_Controller.Input((int)ViveManager.instance.rightHand.GetComponent<SteamVR_TrackedObject>().index);

        if (lDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger) )
        {
            PhotonNetwork.Instantiate(cubePrefab.name, ViveManager.instance.leftHand.transform.position, ViveManager.instance.leftHand.transform.rotation, 0);
        }
        if (rDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            PhotonNetwork.Instantiate(cubePrefab.name, ViveManager.instance.rightHand.transform.position, ViveManager.instance.rightHand.transform.rotation, 0);
        }


    }

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Triggered" + col.gameObject.name);
        if(col.CompareTag("Cube"))
        {
            col.gameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player.ID);
        }
    }
}
