﻿using UnityEngine;
using System.Collections;

public class ChangeOwner : Photon.MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void ChangeOwnership()
    {
        GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.player.ID);
    }


}
